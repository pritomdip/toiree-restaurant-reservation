/*global jQuery:false */
/*global Trr:false */

window.Project = (function (window, document, $, undefined) {
	'use strict';
	var app = {
		init: function () {

			var toireeApi = $('#toiree-api-wrapper');

			if( toireeApi.length ){
				app.fetchWidgetColumnOne();
			}
		},
		fetchWidgetColumnOne: function(){
			var data = {
				'action': 'toiree_restaurant_api',
				'hotel_id' : Trr.hotel_id,
				'nonce' : ''
			};

			wp.ajax.send('toiree_restaurant_api', {
				data: data,
				dataType : 'json',
				success: function (res) {
					if( res.html ){
						$('#toiree-api-wrapper').html(res.html);

						app.setDatePicker();
						$('#trr_submit_btn_id').on('click', app.fetchWidgetColumnTwo);
					}
				},
				error: function (error) {
					$('#toiree-api-wrapper').append('<p>' + error + '</p>');
				}
			});
		},
		setDatePicker: function(){
			var dateToday = new Date();
			var startDate = $('#trr-datepicker').attr('startDate');
			var endDate   = $('#trr-datepicker').attr('endDate');
			var weekStart = $('#trr-datepicker').attr('weekStart');
			
			var dates = $("#trr-datepicker").datepicker({
				showOn: 'both',
		        dateFormat: "dd/mm/yy",
		        buttonImageOnly: true,
		        startDate: startDate,
		        endDate: endDate,
		        weekStart : weekStart,
		        beforeShowDay: app.checkDisabledDates,
		        buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
			    minDate: dateToday,
			    onSelect: function(selectedDate) {
			        var option = this.id == "from" ? "minDate" : "maxDate",
			            instance = $(this).data("datepicker"),
			            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			        dates.not(this).datepicker("option", option, date);

			    }
			});

			if(dates){
				$("#trr-datepicker").datepicker().datepicker("setDate", new Date());
			}

		},
		checkDisabledDates:function(date){

			var disableDates = $('#trr-datepicker').attr('dateDisable');
			var myBadDates   = JSON.parse(disableDates);
			var $return 	 = true;
			var $returnclass = "available";
			var $checkdate 	 = $.datepicker.formatDate('dd/mm/yy', date);

			for( var i = 0; i < myBadDates.length; i++ ){    
		        if( myBadDates[i] == $checkdate ){
			        $return = false;
			        $returnclass= "unavailable";
		        }
		    }

			return [$return,$returnclass];
		},
		fetchWidgetColumnTwo: function(e){

			e.preventDefault();
			$('.trr_modal').toggleClass('is-visible');
			$('.trr_modal-close').on('click', function(){
			  	$(this).parents('.trr_modal').removeClass('is-visible');
			});

			$('#modal_submit_btn').on('click', app.onClickSendApiData);

		},
		onClickSendApiData: function(e){
			e.preventDefault();

			var formValues  = {};
			var form1 		= $('#trr_form').serializeArray(); 
			var form2 		= $('#trr_modal_form').serializeArray();

			$.each( form1, function(i, field) {
			    formValues[field.name] = field.value;
			});

			$.each( form2, function(i, field) {
			    formValues[field.name] = field.value;
			});

			var Phone 		  		= formValues.Phone;
			var Email 		  		= formValues.email; 
			var checkNumberValidity = Number.isInteger( parseInt( formValues.Phone ) );

			if( !checkNumberValidity ){
				alert('Please input numeric characters only');
				return false;
			}

			var checkMail = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			
			if ( !checkMail.test( Email ) ) {
				alert('Please enter a valid email address');
				return false;
			}
		   
			formValues.RestaurantCode = Trr.hotel_id,

			wp.ajax.send('toiree_restaurant_api_step2', {
				type:"POST",
				data: {
					'action'   : 'toiree_restaurant_api_step2',
					'hotel_id' : Trr.hotel_id,
					'formData' : formValues
				},
				dataType : 'json',
				success: function (res) {

					if( res.html ){
						$('.trr_modal_right_content').html(res.html);
						$('#modal_close_btn').on('click', app.exitPopupRefresh);
							// $(this).parents('.trr_modal').removeClass('is-visible');

							// $(this).parents('.trr_modal').removeClass('is-visible').hide();
						
					} else {

					}
				},
				error: function (error) {
					$('.trr_modal_right_content').append('<p>' + error + '</p>');
				}
			});
		},
		exitPopupRefresh:function(e){
			$(this).parents('.trr_modal').removeClass('is-visible').hide();

			e.preventDefault();

			wp.ajax.send('toiree_restaurant_api_exit', {
				type:"POST",
				data: {
					'action'   : 'toiree_restaurant_api_exit',
					'hotel_id' : Trr.hotel_id,
					'nonce' : ''
				},
				dataType : 'json',
				success: function (res) {

					if( res.html ){
						$('.trr_modal_right_content').html(res.html);
					}
				},
				error: function (error) {
					
				}
			});
		}
				
    };
	$(document).ready(app.init);

	return app;

})(window, document, jQuery);
