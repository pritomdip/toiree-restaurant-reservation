<?php

namespace Pritom\ToireeReataurant\Admin;

use Pritom\ToireeReataurant\Admin\Settings_API;

class Settings {
	private $settings_api;

	function __construct() {
		$this->settings_api = new Settings_API();
		add_action('admin_init', array( $this, 'admin_init' ));
		add_action('admin_menu', array( $this, 'admin_menu' ));
	}

	function admin_init() {
		//set the settings
		$this->settings_api->set_sections($this->get_settings_sections());
		$this->settings_api->set_fields($this->get_settings_fields());

		//initialize settings
		$this->settings_api->admin_init();
	}

	function get_settings_sections() {
		$sections = array(
			array(
				'id'    => 'trr_settings',
				'title' => __( 'Restaurant API Settings', 'toiree-restaurant-reservation' )
			),
		);

		return apply_filters( 'trr_settings_sections', $sections );
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	function get_settings_fields() {

		$settings_fields = array(

			'trr_settings' => array(

				array(
					'name'    => 'hotel_id',
					'label'   => __('Hotel Id', 'toiree-restaurant-reservation'),
					'type'    => 'text',
					'default' => '12345',
					'class'   => 'hotel_id_class',
				),

				array(
					'name'    => 'hotel_name',
					'label'   => __('Hotel Name', 'toiree-restaurant-reservation'),
					'type'    => 'text',
					'default' => 'Hotel Holy City Garden',
					'class'   => 'hotel_name_class',
				),

				array(
					'name'    => 'btn_text',
					'label'   => __('Button Text One', 'toiree-restaurant-reservation'),
					'type'    => 'text',
					'default' => 'Reservation',
					'class'   => 'btn_text_class',
				),

				array(
					'name'    => 'btn_text_two',
					'label'   => __('Button Text Two', 'toiree-restaurant-reservation'),
					'type'    => 'text',
					'default' => 'Submit',
					'class'   => 'btn_text_two_class',
				),

				array(
					'name'    => 'btn_text_three',
					'label'   => __('Button Text Three', 'toiree-restaurant-reservation'),
					'type'    => 'text',
					'default' => 'Submit',
					'class'   => 'btn_text_three_class',
				),

				array(
					'name'    => 'btn_bg_clr',
					'label'   => __('Button Background Color', 'toiree-restaurant-reservation'),
					'type'    => 'color',
					'default' => '#000',
					'class'   => 'btn_bg_clr_class',
				),

				array(
					'name'    => 'btn_text_clr',
					'label'   => __('Button Text Color', 'toiree-restaurant-reservation'),
					'type'    => 'color',
					'default' => '#fff',
					'class'   => 'btn_text_clr_class',
				),

			),
		);

		return apply_filters('trr_settings_fields', $settings_fields);
	}

	/**
	 * Add Portfolio Gallary settings sub menu to Portfolio admin menu
	 *
	 * @since 1.0.0
	 */

	function admin_menu() {

	    add_menu_page( 
	        __( 'Trr Settings', 'toiree-restaurant-reservation' ),
	        'Toiree Restaurant Settings',
	        'manage_options',
	        'trr_settings',
	        array( $this, 'settings_page' ),
	        'dashicons-admin-tools',
	        6
	    ); 

	}

	/**
	 * Menu page for Portfolio Gallery sub menu
	 *
	 * @since 1.0.0
	 */

	function settings_page() {

		echo '<div class="wrap">';
		$this->settings_api->show_settings();

		$options = get_option('trr_settings');
		if( $options != '' ){
			echo '<p>Place the shortcode and you will get the form.</p>';
			echo '<h1 style="margin-top: 0px;">do_shortcode[toiree_restaurant_reservation_api]</h1>';
		}

		echo '</div>';
	}
}




