<?php

namespace pritom\ToireeReataurant;

class Restaurant_settings_form {
    /**
     * API constructor.
    */
    public function __construct() {

        add_action('wp_ajax_toiree_restaurant_api', array( $this, 'get_toiree_restaurant_api_settings'));
		add_action('wp_ajax_nopriv_toiree_restaurant_api', array( $this, 'get_toiree_restaurant_api_settings') );

		add_action('wp_ajax_toiree_restaurant_api_step2', array( $this, 'toiree_restaurant_api_step2') );
		add_action('wp_ajax_nopriv_toiree_restaurant_api_step2', array( $this, 'toiree_restaurant_api_step2') );

        add_action('wp_ajax_toiree_restaurant_api_exit', array( $this, 'toiree_restaurant_api_exit') );
        add_action('wp_ajax_nopriv_toiree_restaurant_api_exit', array( $this, 'toiree_restaurant_api_exit') );
    }

    public function get_toiree_restaurant_api_settings(){

    	$hotel_id = $_POST['hotel_id'];

    	if( empty($hotel_id ) ){
    		wp_send_json_error( 'Restaurant Id not given. Please Provide it.' );
    	}

		$url      = "http://rbe.eatnearby.nl/Api/BookingForm/". $hotel_id;

		$data 	  = wp_remote_get( esc_url_raw( $url ) );

		if( is_wp_error( $data ) ) {
			return false;
		}

		$result   = json_decode( wp_remote_retrieve_body( $data ), true );

    	if ( empty( $result ) || empty( $result['RestaurantName']) ){
			wp_send_json_error( 'Invalid Restaurant ID' );
    	}

    	if ( $result['FormStyle'] !== 'Widget' ){
			wp_send_json_error( 'Widget Style Not Selected' );
    	}

    	if( !empty( $result['Times'] ) ){
    		$times = $result['Times'];
    	} else {
    		wp_send_json_error( 'Time is not provided in the api yet' );
    	}

    	if( !empty( $result['NumberOfPersons'] ) ){
    		$person_numbers = $result['NumberOfPersons'];    		
    	} else {
    		wp_send_json_error( 'Number of Person is not provided in the api yet' );
    	}

    	if( $result['BookingEngineLogo'] != ''){
    		$image = trr_base64_to_jpeg( $result['BookingEngineLogo'], 'logo.jpg');
    	} else {
    		$image = '';
    	}

    	if( $result['RestaurantLogo'] != ''){
    		$restaurantLogo = trr_base64_to_jpeg( $result['RestaurantLogo'], 'reslogo.jpg');
    	} else {
    		$restaurantLogo = '';
    	}

    	if( $result['BookingFeaturedImagePortrait'] != ''){
    		$feature_image = trr_base64_to_jpeg( $result['BookingFeaturedImagePortrait'], 'feature_image.jpg');
    	} else {
    		$feature_image = '';
    	}

    	$bookingCalender   = $result['BookingCalenderVM'];

    	if( !empty( $bookingCalender ) ){
    		$dateDisabled  = json_encode( $bookingCalender['DatesDisabled'] );
    		$weekStart	   = $bookingCalender['WeekStart'];
    		$startDate	   = $bookingCalender['StartDate'];
    		$endDate	   = $bookingCalender['EndDate'];
    	} else {
    		wp_send_json_error( 'Date has an issue in api' );
    	}

    	$required_fields   = $result['BookingSettingRequireVm'];

    	$settings 		   = get_option('trr_settings');

    	ob_start();

		include TRR_TEMPLATES_DIR . '/template-form1.php';

		$html = ob_get_clean();
		ob_get_clean();

		wp_send_json_success([
			'html' => $html,
		]);
    }

    public function toiree_restaurant_api_step2(){

    	$hotel_id = $_POST['hotel_id'];

    	if( empty( $hotel_id ) ){
    		wp_send_json_error( 'Restaurant Id not given. Please Provide it.' );
    	}

		$url      	 = "http://rbe.eatnearby.nl/Api/BookingForm/". $hotel_id;
    	$data 		 = $_POST['formData'];
    	$jsonData	 = json_encode($data);

		$response 	 = wp_remote_post( esc_url_raw( $url ), array(
			    'method' 	  => 'POST',
			    'headers' 	  => array('Content-Type' => 'application/json' ),
			    'body' 		  => $jsonData
		    )
		);

		if ( is_wp_error( $response ) || $response['response']['code'] != 200 ) {
			wp_send_json_error( 'Something went wrong. Please try again later' );
		}

		$result  = json_decode( wp_remote_retrieve_body( $response ), true );

		if ( empty( $result ) || empty( $result['RestaurantCode']) ){
			wp_send_json_error( 'Invalid Restaurant ID' );
    	}

		if( $result['ResponseMessaageAfterReservation'] != '' ){
			$responseMessage = $result['ResponseMessaageAfterReservation'];
		} else {
			$responseMessage = '';
		}

		if( $result['WarningMessage'] != '' ){
			$WarningMessage = $result['WarningMessage'];
		} else {
			$WarningMessage = '';
		}

        if( $result['RestaurantLogo'] != ''){
            $restaurantLogo = trr_base64_to_jpeg( $result['RestaurantLogo'], 'reslogo.jpg');
        } else {
            $restaurantLogo = '';
        }
        $getDate            = $result['Date'];
        $showDate           = date("d/m/Y", strtotime($getDate));

		$settings           = get_option('trr_settings');
		
    	ob_start();

		include TRR_TEMPLATES_DIR . '/template-form2.php';

		$html = ob_get_clean();
		
		ob_get_clean();

		wp_send_json_success([
			'html' => $html,
		]);
    }

    public function toiree_restaurant_api_exit(){

        $hotel_id = $_POST['hotel_id'];

        $url      = "http://rbe.eatnearby.nl/Api/BookingForm/". $hotel_id;

        $data     = wp_remote_get( esc_url_raw( $url ) );

        if( is_wp_error( $data ) ) {
            return false;
        }

        $result   = json_decode( wp_remote_retrieve_body( $data ), true );

        $settings = get_option('trr_settings');
        
        ob_start();

        include TRR_TEMPLATES_DIR . '/form.php';

        $html = ob_get_clean();
        ob_get_clean();

        wp_send_json_success([
            'html' => $html,
        ]);

    }

}