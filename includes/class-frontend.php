<?php

namespace pritom\ToireeReataurant;

class Frontend {
	/**
	 * The single instance of the class.
	 *
	 * @var Frontend
	 * @since 1.0.0
	 */
	protected static $init = null;

	/**
	 * Frontend Instance.
	 *
	 * @since 1.0.0
	 * @static
	 * @return Frontend - Main instance.
	 */
	public static function init() {
		if ( is_null( self::$init ) ) {
			self::$init = new self();
			self::$init->setup();
		}

		return self::$init;
	}

	/**
	 * Initialize all frontend related stuff
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function setup() {
		$this->includes();
		$this->init_hooks();
		$this->instance();
	}

	/**
	 * Includes all frontend related files
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
		require_once dirname( __FILE__ ) . '/class-shortcodes.php';
		require_once dirname( __FILE__ ) . '/class-form-ajax.php';
	}

	/**
	 * Register all frontend related hooks
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function init_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Fire off all the instances
	 *
	 * @since 1.0.0
	 */
	protected function instance() {
		new Shortcode();
		new Restaurant_settings_form();
	}

	/**
	 * Loads all frontend scripts/styles
	 *
	 * @param $hook
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function enqueue_scripts( $hook ) {

		wp_enqueue_script('jquery');
		wp_enqueue_script( 'jquery-ui-datepicker' );
		
		wp_register_style('toiree-restaurant-reservation', TRR_ASSETS_URL."/css/frontend.css", TRR_VERSION);
		//wp_register_style('toiree-jquery-ui', TRR_ASSETS_URL."/css/jquery-ui.css", TRR_VERSION);
		wp_register_style('toiree-jquery-ui', "http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css", TRR_VERSION);

		wp_register_script('toiree-restaurant-reservation', TRR_ASSETS_URL."/js/frontend/frontend.js", ['jquery', 'wp-util'], TRR_VERSION, true);

		wp_enqueue_style('toiree-restaurant-reservation');
		wp_enqueue_style('toiree-jquery-ui');
		wp_enqueue_script('toiree-restaurant-reservation');

		$hotel_id = trr_get_option_settings( 'trr_settings', 'hotel_id' );
		$hotel_id = empty( $hotel_id ) ? '' : $hotel_id;

		wp_localize_script('toiree-restaurant-reservation', 'Trr', array(
			'ajaxurl'   => admin_url('admin-ajax.php'),
			'hotel_id'  => $hotel_id,
			'nonce'     => wp_create_nonce( 'toiree-restaurant-reservation' )
		));		
			
	}

}

Frontend::init();
