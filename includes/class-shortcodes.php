<?php

namespace pritom\ToireeReataurant;

class Shortcode {
    /**
     * Shortcode constructor.
    */
    public function __construct() {	
        add_shortcode( 'toiree_restaurant_reservation_api', array( $this, 'render_toiree_restaurant_reservation__form' ) );
    }

    /**
     * Render Shortcode.
     *  @since 1.0.0
	 * @return string
    */

    public function render_toiree_restaurant_reservation__form($attr){

        $options = get_option('trr_settings');
		if( $options == '' ){
			return;
		}

		ob_start();
        echo '<div id="toiree-api-wrapper"></div>';
		$html = ob_get_contents();
		ob_get_clean();
		return $html; 
    }     
}