<?php
//function prefix trr

function trr_get_option_settings( $option_name = '', $args = '' ){
	if( empty( $args ) || empty( $option_name ) ){
		return;
	}

	$settings = get_option( $option_name );

	if( empty( $settings ) || empty( $settings[$args] ) ){
		return;
	}

	return $settings[$args];
}


function trr_base64_to_jpeg($base64_string, $output_file) {

    $ifp  = fopen( $output_file, 'wb' );

    $data = explode( ',', $base64_string );

    fwrite( $ifp, base64_decode( $data[ 0 ] ) );

    fclose( $ifp );
	
    return $output_file; 
}

