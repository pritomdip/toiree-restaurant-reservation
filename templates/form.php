<div class="trr-logo-name-wrapper">

    <div class="trr-restaurant-logo-wrapper">
        <?php $logo_url = admin_url() . 'reslogo.jpg'; ?>
        <img src="<?php echo $logo_url; ?>"/>
    </div>

    <div class="trr-restaurant-name-wrapper">
        <h3 class="modal_trr_restaurant_name"><?php echo esc_html( $result['RestaurantName'] ); ?></h3>
    </div>

    <p class="trr_warning_msg"><?php echo esc_html( $result['WarningMessage'] ); ?></p>

</div>

<form class="trr_modal_form" id="trr_modal_form">
	<div class="trr_modal_row">
		<label><?php _e('First Name: ', 'toiree-reataurant-reservation') ?></label>
		<input type="text" name="FirstName" class="trr_modal_input" <?php echo $required_fields['FirstNameRequirement'] ? 'required' : ''; ?>/>
	</div>

	<div class="trr_modal_row">
		<label><?php _e('Last Name: ', 'toiree-reataurant-reservation') ?></label>
		<input type="text" name="LastName" class="trr_modal_input" <?php echo $required_fields['LastNameRequirement'] ? 'required' : ''; ?>/>
	</div>

	<div class="trr_modal_row">
		<label><?php _e('Phone: ', 'toiree-reataurant-reservation') ?></label>
		<input type="text" name="Phone" class="trr_modal_input" <?php echo $required_fields['PhoneRequirement'] ? 'required' : ''; ?> />
	</div>

	<div class="trr_modal_row">
		<label><?php _e('Email: ', 'toiree-reataurant-reservation') ?></label>
		<input type="email" name="email" class="trr_modal_input" <?php echo $required_fields['EmailRequirement'] ? 'required' : ''; ?>/>
	</div>

    <div class="trr_modal_row">
        <label><?php _e('Message Box: ', 'toiree-reataurant-reservation') ?></label>
        <textarea name="MessageBox" class="trr-form-textarea trr_modal_input"></textarea>
    </div>

    <div class="trr_modal_row trr_center">

        <button type="submit" id="modal_submit_btn" class="trr_modal_submit_btn" style="background:<?php echo $settings['btn_bg_clr']; ?>; color:<?php echo $settings['btn_text_clr']; ?>"><?php echo esc_html($settings['btn_text_two'], 'toiree-restaurant-reservation'); ?></button>

    </div>

</form>