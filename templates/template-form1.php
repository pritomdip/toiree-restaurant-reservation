<div id="trr_reservation">
    <div class="trr_reservation_top">
    </div>

    <div class="trr_reservation_body">

        <div id="trr_wrapper">
            
            <form method="POST" action="#" id="trr_form">
                <div id="trr_fotrr_wrapper">
                    <h1 class="trr_header"><?php echo esc_html($result['RestaurantName']); ?></h1>
                    <div class="trr_box_wrapper">
                        <div class="trr_gray_box">
                        	<?php if(!empty($result['CustomText']) || $result['CustomText']){ ?>
                            <p class="trr_notification_text"><?php echo esc_html($result['CustomText']); ?></p>
                        	<?php } ?>
                            <div id="trr_search_box">
                                <div class="trr_box_row trr_date_row">
                                    <div class="trr_row_title"><?php _e('Date:', 'toiree-restaurant-reservation') ?></div>
                                    <div class="trr_row_value">
                                        <input type="text" id="trr-datepicker" class="trr-date" name="Date" dateDisable='<?php echo $dateDisabled; ?>' startDate = '<?php echo $startDate; ?>' endDate = '<?php echo $endDate; ?>' weekStart='<?php echo $weekStart; ?>' <?php echo $required_fields['DateRequirement'] ? 'required' : ''; ?>/>
                                    </div>
                                </div>
                                <div class="trr_box_row">
                                    <div class="trr_row_title"><?php _e('Persons:', 'toiree-restaurant-reservation') ?></div>
                                    <div class="trr_row_value">
                                        <select name="NumberOfPerson">
                                            <?php foreach( $person_numbers as $person_number ){ ?>
												<option value="<?php echo $person_number['Value']; ?>"><?php echo $person_number['Text']; ?></option>
											<?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="trr_box_row">
                                    <div class="trr_row_title"><?php _e('Time:', 'toiree-restaurant-reservation') ?></div>
                                    <div id="divTime" class="trr_row_value">
                                        <select class="selectTime" id="selectTime" name="Time" <?php echo $required_fields['TimeRequirement'] ? 'required' : ''; ?>>
                                            <?php foreach( $times as $time ){ ?>
												<option value="<?php echo $time['Value']; ?>"><?php echo $time['Text']; ?></option>
											<?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <button type="submit" id="trr_submit_btn_id" class="trr_submit_btn" style="background:<?php echo $settings['btn_bg_clr']; ?>; color:<?php echo $settings['btn_text_clr']; ?>"><?php echo esc_html($settings['btn_text'], 'toiree-restaurant-reservation'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div id="trr_logo_wrapper"><span class="trr_powered">powered by</span>

            	<?php $logo_url = admin_url() . 'logo.jpg'; ?>

                <a alt="restaurants" style="background-image:url(<?php echo $logo_url; ?>)" class="trr_logo" href="javascript:void(0)"></a>

            </div>
        </div>
    </div>
    <div class="trr_reservation_bottom">
    </div>
</div>


<div class="trr_modal">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
        <div class="modal-header">
          	<button class="trr_modal-close modal-toggle"> X </button>
        </div>
        
        <div class="modal-body">
          	<div class="modal-content abcde_check">
            	<div class="trr_modal_left_content">
            		<?php $featured_image_url = admin_url() . 'feature_image.jpg'; ?>
            		<img src="<?php echo $featured_image_url; ?>"/>

                    <div id="trr_logo_wrapper"><span class="trr_powered">powered by</span>

                        <?php $logo_url = admin_url() . 'logo.jpg'; ?>

                        <a alt="restaurants" style="background-image:url(<?php echo $logo_url; ?>)" class="trr_logo" href="javascript:void(0)"></a>

                    </div>


            	</div>
            	<div class="trr_modal_right_content">

                    <?php include( TRR_TEMPLATES_DIR . '/form.php'); 
                    // var_dump(plugin_dir_path( __FILE__ ));
                    ?>

            	</div>
          	</div>
        </div>
    </div>
</div>