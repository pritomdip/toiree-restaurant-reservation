<div class="trr-logo-name-wrapper">

    <div class="trr-restaurant-logo-wrapper">
        <?php $logo_url = admin_url() . 'reslogo.jpg'; ?>
        <img src="<?php echo $logo_url; ?>"/>
    </div>

    <div class="trr-restaurant-name-wrapper">
        <h3 class="modal_trr_restaurant_name"><?php echo esc_html( $result['RestaurantName'] ); ?></h3>
    </div>

    <p class="trr_warning_msg"><?php echo esc_html( $result['WarningMessage'] ); ?></p>

</div>

<div class="trr-step2">
	<p><?php echo esc_html( $responseMessage ); ?></p>
	<div class="trr-form-values">
		<p>Name : <?php echo esc_html( $result['FirstName'] . ' ' . $result['LastName'] ) ?></p>
		<p>Date : <?php echo esc_html( $showDate  ) ?></p>
		<p>Time : <?php echo esc_html( $result['Time']  ) ?></p>
		<p>Number Of Person : <?php echo esc_html( $result['NumberOfPerson']  ) ?></p>
		<p>Email : <?php echo esc_html( $result['Email']  ) ?></p>
		<p>Phone : <?php echo esc_html( $result['Phone']  ) ?></p>

		<?php if( $result['MessageBox'] != '' ){ ?>
			<p>Message : <?php echo esc_html( $result['MessageBox']  ) ?></p>
		<?php } ?>

		<button type="submit" id="modal_close_btn" class="trr_modal_submit_btn trr_margin-t10" style="background:<?php echo $settings['btn_bg_clr']; ?>; color:<?php echo $settings['btn_text_clr']; ?>"><?php echo esc_html($settings['btn_text_three'], 'toiree-restaurant-reservation'); ?></button>
		
	</div>
</div>