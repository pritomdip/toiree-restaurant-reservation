<?php
/**
 * EDDNotification Uninstall
 *
 * Uninstalling EDDNotification deletes user roles, pages, tables, and options.
 *
 * @package EDDNotification\Uninstaller
 * @version 1.0.0
 */

defined( 'WP_UNINSTALL_PLUGIN' ) || exit;
